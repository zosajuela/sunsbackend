const express = require('express');
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')
//lets identify the needed dependencies to create our routes.


//[SECTION] Primary Routes
router.post('/email-exists', (req, res) => {
   //inside the body section lets describe what will be the procedure upon sending a request ib the route.
   UserController.emailExists(req.body).then(result => res.send(result));
})


//register a new user
router.post('/register', (req, res) => {
    UserController.register(req.body).then(result => res.send(result));
})

//lets create a route for our login method.
router.post('/login', (req, res) => {
  UserController.login(req.body).then(result => res.send(result))
})

//lets create a route that will send a request to display the user's information.
//upon getting the user, the user has to be authenticated to determine the proper authorization rights for the user
router.get('/details', auth.verify, (req, res) => {
   //get the headers section of the request with the JWT.
   const user = auth.decode(req.headers.authorization) //we are stripping away the header section of the data to decode the token to get the payload.
   UserController.get({ userId: user.id }).then(user => res.send(user))
}) 


router.post('/enroll', auth.verify, (req, res) => {
   const params = {
   	  userId: auth.decode(req.headers.authorization).id, 
      courseId: req.body.courseId
   } //we capturing the data. 
   UserController.enroll(params).then(result => res.send(result)) 
})
//add an object inside the enrollments array
//the user should be logged in and should have the correct access rights first before being allowed to enroll. (you need to verify the indentity of the user first).
module.exports = router; 
