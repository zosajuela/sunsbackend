const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan')
const app = express()
const path = require('path')

const cors = require('cors')

// const corsOptions = {
// 	origin: 'https://evening-fjord-91994.herokuapp.com/',
// 	 optionsSuccessStatus: 200
//  }

app.use(cors())

require('dotenv').config()

const port = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

mongoose.connect(connectionString, {
	useCreateIndex: true,
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

app.use(morgan('tiny'));

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// app.get('/', (req,res) => {
// 	res.send("successfully hosted")
// })

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open',()=> console.log("Now connected to MongoDB Atlas DataBase"));


app.set("view engine","ejs")
//css/style.css 
app.use('/css', express.static(path.resolve(__dirname,"assets/css")))
app.use('/img', express.static(path.resolve(__dirname,"assets/img")))
app.use('/js', express.static(path.resolve(__dirname,"assets/js")))
//css/style.css 

const studentRouter = require('./server/router/router')

app.use('/', studentRouter)

app.get('/', function(req,res){
	res.send(`you are now connected on port ${port}`);
})


//we need to define the routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')

app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)

app.listen(port, ()=> console.log(`you got served on port ${port}`));

