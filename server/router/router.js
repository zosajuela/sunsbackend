const express = require('express');
const router = express.Router()

const services = require('../services/render');
const controller = require('../controller/controller');

router.get('/',services.homeRoutes)



router.get('/add-student',services.add_student)

router.get('/update-student',services.update_student)

router.post('/api/student',controller.create);
router.get('/api/student',controller.find);
router.put('/api/student/:id',controller.update);
router.delete('/api/student/:id',controller.delete);

module.exports = router