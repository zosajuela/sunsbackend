const mongoose = require('mongoose')

//we are now going to create the schema for our users
const userSchema = new mongoose.Schema({
     firstName: {
     	type: String,
     	required: [true, 'First name is required']
     },
     lastName: {
     	type: String,
     	required: [true, 'Last name is required']
     }, 
     email: {
     	type: String,
     	required: [true, 'email is required']
     },
     password: {
     	type: String, 
     	required: [true, 'password is required']
     }, 
     isAdmin: {
     	type: Boolean, 
     	default: false 
     }, 
     mobileNo: {
     	type: String, 
     	required: [true, 'Mobile number is required']
     }, 
     enrollments: [
        {
	     	courseId: {
	     		type: String, 
	     		required: [true, 'Course Id is required']
	     	}, 
	     	enrolledOn: {
	     		type: Date, 
	     		defaullt: new Date()
	     	},
	     	status: {
	     		type: String,
	     		default: 'Enrolled' //Alternative values are 'cancelled and Completed'
	     	}
	    }
     ]
}) 

//Now that we have defines the schema for the users lets now define the controller for the users

module.exports = mongoose.model('user', userSchema)
